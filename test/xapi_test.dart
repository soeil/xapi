import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:xapi/xapi.dart';

void main() {
  const MethodChannel channel = MethodChannel('xapi');

  TestWidgetsFlutterBinding.ensureInitialized();

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      return '42';
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  test('getPlatformVersion', () async {
    expect(await Xapi.platformVersion, '42');
  });
}
