#import "XapiPlugin.h"
#if __has_include(<xapi/xapi-Swift.h>)
#import <xapi/xapi-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "xapi-Swift.h"
#endif

@implementation XapiPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftXapiPlugin registerWithRegistrar:registrar];
}
@end
