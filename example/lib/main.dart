import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';
import 'package:xapi/basicSetting.dart';
import 'package:xapi/xapi.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _platformVersion = 'Unknown';

  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    String platformVersion;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      platformVersion = await Xapi.platformVersion;
    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    Xapi.setConfig(BasicSetting('https://tribelio.app.ittron.co.id/', 5000, 3000));

    setState(() {
      _platformVersion = platformVersion;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Center(
          child: GestureDetector(
            onTap: () async {
              Xapi xapi = Xapi();

              xapi.onErrorResponse = (String message, String type, response) {
                print(message);
              };
              xapi.onReceiveResponse = (dynamic response) {};
              xapi.get(
                "api/member/commerceAdmin/GetProduct?lang=en_US&networkId=859&page=1&lat=-7.2681467&statusActive=all&devices=android&isMobile=1&sessionId=20210218180520602e49f092e41&networkAccountId=198&lng=112.7344126",
                onReceiveProgress: (a, b) {
                  print('onReceiveProgress $a $b');
                },
              );

              var result = await FilePicker.getFile();

              if (result != null) {
                File file = File(result.path);
                xapi.post(
                  'api/',
                  data: {
                    'file': xapi.fromFile(file.path, 'aaa.jpg'),
                  },
                  onSendProgress: (int a, int b) {
                    print('onSendProgress $a $b');
                  }
                );
              } else {
                // User canceled the picker
              }
              getApplicationDocumentsDirectory().then((value) {
                print('${value.path}/x.html');
                xapi.download('https://www.google.com/', '${value.path}/x.html', onReceiveProgress: (int a, int b) {
                  print('onSendProgress $a $b');
                });
              });
            },
            child: Text('Running on: $_platformVersion\n'),
          ),
        ),
      ),
    );
  }
}
