# xapi

a dio wraper

## Get started

### Add dependency

```yaml
xapi:
    git:
      url: https://soeil@bitbucket.org/soeil/xapi.git
```

### Set file configuration

```dart
Xapi.setConfig(BasicSetting('https://tribelio.app.ittron.co.id/', 5000, 3000));
```

### Set callback for error response and on receive response

```dart
xapi.onErrorResponse = (String message, String type, response) {
    print(message);
  };
  xapi.onReceiveResponse = (dynamic response) {};
```

### GET Request

```dart
Xapi xapi = Xapi();

  xapi.onErrorResponse = (String message, String type, response) {
    print(message);
  };
  xapi.onReceiveResponse = (dynamic response) {};
  xapi.get(
    "api/member/commerceAdmin/GetProduct?lang=en_US&networkId=859&page=1&lat=-7.2681467&statusActive=all&devices=android&isMobile=1&sessionId=20210218180520602e49f092e41&networkAccountId=198&lng=112.7344126",
    onReceiveProgress: (a, b) {
      print('onReceiveProgress $a $b');
    },
  );
```

### POST Request

```dart
xapi.post(
  'api/',
  data: {
    'file': xapi.fromFile(file.path, 'aaa.jpg'),
  },
  onSendProgress: (int a, int b) {
    print('onSendProgress $a $b');
  }
);
```

### PUT Request

```dart
xapi.put(
  'api/',
  data: {
    'file': xapi.fromFile(file.path, 'aaa.jpg'),
  },
  onSendProgress: (int a, int b) {
    print('onSendProgress $a $b');
  }
);
```

### DELETE Request

```dart
Xapi xapi = Xapi();

  xapi.onErrorResponse = (String message, String type, response) {
    print(message);
  };
  xapi.onReceiveResponse = (dynamic response) {};
  xapi.get(
    "api/member/commerceAdmin/GetProduct?lang=en_US&networkId=859&page=1&lat=-7.2681467&statusActive=all&devices=android&isMobile=1&sessionId=20210218180520602e49f092e41&networkAccountId=198&lng=112.7344126",
    onReceiveProgress: (a, b) {
      print('onReceiveProgress $a $b');
    },
  );
```

### Download Request

```dart
xapi.download('https://www.google.com/', '${value.path}/x.html', onReceiveProgress: (int a, int b) {
  print('onSendProgress $a $b');
});
```