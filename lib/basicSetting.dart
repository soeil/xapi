class BasicSetting {
  String baseUrl = '';
  int connectTimeout = 0;
  int receiveTimeout = 0;
  bool useCappFormatResponse = true;
  bool showTechnicalError = true;
  bool debug = true;

  BasicSetting(this.baseUrl, this.connectTimeout, this.receiveTimeout, {showTechnicalError, useCappFormatResponse, debug}) {
    this.useCappFormatResponse = useCappFormatResponse;
    if (this.useCappFormatResponse == null) {
      this.useCappFormatResponse = true;
    }
    this.showTechnicalError = showTechnicalError;
    if (this.showTechnicalError == null) {
      this.showTechnicalError = true;
    }
    this.debug = debug;
    if (this.debug == null) {
      this.debug = true;
    }
  }
}