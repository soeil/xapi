import 'dart:async';
import 'dart:io';

import 'package:dio/adapter.dart';
import 'package:dio/dio.dart';
import 'package:flutter/services.dart';
import 'package:logger/logger.dart';

import 'basicSetting.dart';

BasicSetting config;
String certificateFile;
String certificatePassword;

typedef ResponseCallback = void Function(Response response);
typedef ErrorResponseCallback = void Function(String message, String code, Response response);

class Xapi {
  CancelToken _token;
  ResponseCallback onReceiveResponse;
  ErrorResponseCallback onErrorResponse;
  bool _useCappResponse;
  String _singleUseUrl;
  static Logger _logger;

  static const MethodChannel _channel = const MethodChannel('xapi');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  Xapi({bool useCappResponse, String singleUseUrl}) {
    _token = CancelToken();
    _useCappResponse = useCappResponse;
    _singleUseUrl = singleUseUrl;
    if (_useCappResponse == null) {
      _useCappResponse = config.useCappFormatResponse;
    }
    _logger = Logger(
      printer: PrettyPrinter(
        methodCount: 0,
        // number of method calls to be displayed
        errorMethodCount: 8,
        // number of method calls if stacktrace is provided
        lineLength: 120,
        // width of the output
        colors: false,
        // Colorful log messages
        printEmojis: true,
        // Print an emoji for each log message
        printTime: false, // Should each log print contain a timestamp),
      ),
    );
  }

  static void setConfig(BasicSetting newConfig) => config = newConfig;

  static void setCertificate(String file, String password) {
    certificateFile = file;
    certificatePassword = password;
  }

  void download(String url, String path, {Map<String, String> queryParameters, ProgressCallback onReceiveProgress}) async {
    if (_basicCheck()) {
      return;
    }

    _log(queryParameters);

    try {
      Dio dio = new Dio();

      Response response = await dio.download(
        url,
        path,
        queryParameters: queryParameters,
        cancelToken: _token,
        onReceiveProgress: onReceiveProgress,
      );

      _log(response.realUri.toString());
      _log(response.statusCode);

      if (onReceiveResponse != null) {
        onReceiveResponse(response);
      }
    } on DioError catch (e) {
      _log(queryParameters);
      _log(e.message);

      _technicalError(e.message);
    } on SocketException catch (_) {
      _technicalError('Please check internet connection');
    } catch (e) {
      _technicalError(e.message);
    }
  }

  void get(String path, {Map<String, String> queryParameters, ProgressCallback onReceiveProgress}) async {
    if (_basicCheck()) {
      return;
    }

    _log(queryParameters);

    try {
      Dio dio = new Dio();
      _setOption(dio);

      Response response = await dio.get(
        path,
        queryParameters: queryParameters,
        cancelToken: _token,
        onReceiveProgress: onReceiveProgress,
      );

      _log(response.realUri.toString());
      _log(response.statusCode);
      _log(response.data);

      _getResponse(response);
    } on DioError catch (e) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx and is also not 304.
      _log(queryParameters);
      if (e.response != null) {
        _log(e.response.statusCode);
        _log(e.response.data);

        _getResponse(e.response);
      } else {
        // Something happened in setting up or sending the request that triggered an Error
        _log(e.message);

        _technicalError(e.message);
      }
    } on SocketException catch (_) {
      _technicalError('Please check internet connection');
    } catch (e) {
      _technicalError(e.message);
    }
  }

  void post(String path, {Map<String, dynamic> data, Map<String, String> queryParameters, ProgressCallback onReceiveProgress, ProgressCallback onSendProgress}) async {
    if (_basicCheck()) {
      return;
    }

    _log(queryParameters);

    try {
      Dio dio = new Dio();
      _setOption(dio);

      var dataMap = FormData.fromMap(data ?? {});

      Response response = await dio.post(
        path,
        data: dataMap,
        queryParameters: queryParameters,
        cancelToken: _token,
        onReceiveProgress: onReceiveProgress,
        onSendProgress: onSendProgress,
      );

      _log(response.realUri.toString());
      _log(response.statusCode);
      _log(response.data);

      _getResponse(response);
    } on DioError catch (e) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx and is also not 304.
      _log(queryParameters);
      if (e.response != null) {
        _log(e.response.statusCode);
        _log(e.response.data);

        _getResponse(e.response);
      } else {
        // Something happened in setting up or sending the request that triggered an Error
        _log(e.message);

        _technicalError(e.message);
      }
    } on SocketException catch (_) {
      _technicalError('Please check internet connection');
    } catch (e) {
      _technicalError(e.message);
    }
  }

  void put(String path, {Map<String, dynamic> data, Map<String, String> queryParameters, ProgressCallback onReceiveProgress, ProgressCallback onSendProgress}) async {
    if (_basicCheck()) {
      return;
    }

    _log(queryParameters);

    try {
      Dio dio = new Dio();
      _setOption(dio);

      var dataMap = FormData.fromMap(data ?? {});

      Response response = await dio.put(
        path,
        data: dataMap,
        queryParameters: queryParameters,
        cancelToken: _token,
        onReceiveProgress: onReceiveProgress,
        onSendProgress: onSendProgress,
      );

      _log(response.realUri.toString());
      _log(response.statusCode);
      _log(response.data);

      _getResponse(response);
    } on DioError catch (e) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx and is also not 304.
      _log(queryParameters);
      if (e.response != null) {
        _log(e.response.statusCode);
        _log(e.response.data);

        _getResponse(e.response);
      } else {
        // Something happened in setting up or sending the request that triggered an Error
        _log(e.message);

        _technicalError(e.message);
      }
    } on SocketException catch (_) {
      _technicalError('Please check internet connection');
    } catch (e) {
      _technicalError(e.message);
    }
  }

  void delete(String path, {Map<String, dynamic> data, Map<String, String> queryParameters}) async {
    if (_basicCheck()) {
      return;
    }

    _log(queryParameters);

    try {
      Dio dio = new Dio();
      _setOption(dio);

      var dataMap = FormData.fromMap(data ?? {});

      Response response = await dio.delete(
        path,
        data: dataMap,
        queryParameters: queryParameters,
        cancelToken: _token,
      );

      _log(response.realUri.toString());
      _log(response.statusCode);
      _log(response.data);

      _getResponse(response);
    } on DioError catch (e) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx and is also not 304.
      _log(queryParameters);
      if (e.response != null) {
        _log(e.response.statusCode);
        _log(e.response.data);

        _getResponse(e.response);
      } else {
        // Something happened in setting up or sending the request that triggered an Error
        _log(e.message);

        _technicalError(e.message);
      }
    } on SocketException catch (_) {
      _technicalError('Please check internet connection');
    } catch (e) {
      _technicalError(e.message);
    }
  }

  /// Cancel the request
  void cancel() {
    _token.cancel('');
  }

  Future<MultipartFile> fromFile(String path, String fileName) async {
    final MultipartFile multipartFile = await MultipartFile.fromFile(path, filename: fileName);
    return multipartFile;
  }

  Future<MultipartFile> fromFileSync(String path, String fileName) async {
    final MultipartFile multipartFile = MultipartFile.fromFileSync(path, filename: fileName);
    return multipartFile;
  }

  void _setOption(Dio dio) {
    if (_singleUseUrl != null) {
      dio.options.baseUrl = _singleUseUrl;
    } else {
      dio.options.baseUrl = config.baseUrl;
    }

    dio.options.connectTimeout = config.connectTimeout; //5s
    dio.options.receiveTimeout = config.receiveTimeout;

    if (certificateFile != null) {
      (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate = (HttpClient client) {
        SecurityContext sc = new SecurityContext();
        if (certificatePassword != null) {
          sc.setTrustedCertificates(certificateFile, password: certificatePassword);
        } else {
          sc.setTrustedCertificates(certificateFile);
        }
        HttpClient httpClient = new HttpClient(context: sc);
        return httpClient;
      };
    } else {
      //CERTIFICATE_VERIFY_FAILED
      (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate = (HttpClient client) {
        client.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
        return client;
      };
    }
  }

  bool _basicCheck() {
    if (config == null) {
      _log('First set Basic config');

      return true;
    }

    return false;
  }

  void _technicalError(String message) {
    if (!config.showTechnicalError) {
      message = 'Please check your network connection';
    }
    if (onErrorResponse != null) {
      onErrorResponse(message, '999', null);
    }
  }

  void _getResponse(Response response) {
    if (response == null) {
      if (onErrorResponse != null) {
        onErrorResponse('Empty response', '999', response);
      }
      return;
    }
    if (response.data == null) {
      if (onErrorResponse != null) {
        onErrorResponse('Empty response', '999', response);
      }
      return;
    }

    if (response.data is Map) {
    } else {
      if (onErrorResponse != null) {
        onErrorResponse('Unexpected server response', '999', response);
      }
      return;
    }

    if (_useCappResponse) {
      _getCappResponse(response);
    } else {
      if (onReceiveResponse != null) {
        onReceiveResponse(response);
      }
    }
  }

  void _getCappResponse(Response response) {
    Map json = response.data;

    try {
      if ('${json['errCode']}' == '0') {
        if (onReceiveResponse != null) {
          onReceiveResponse(response);
        }
      } else {
        if (onErrorResponse != null) {
          onErrorResponse(json['errMessage'], '${json['errCode']}', response);
        }
      }
    } catch (e) {
      if (onErrorResponse != null) {
        onErrorResponse('Cannot parse json', '999', response);
      }
    }
  }

  void _log(dynamic value) {
    if (config.debug) {
      _logger.i(value);
    }
  }
}
